RealEyesDemo Application


Creating and developing the RealEyesDemo application was a very fun and good learning experience. The entire application took me roughly a little over twelve and a half hours. This doesn’t include the time it took to test functionality of parts of the application or the time for researching. 

While doing the development of this application I was able to log issues and time completions for each individual issue/bug. I had originally done the tracking in a word document but realized that bitbucket has the functionality built into their application. To see the issues I created/logged select “Issues” in the left hand side menu. 


How to Install
There are two ways to install this application.

1. You can clone and download this repository and install the project into Android Studio on your desktop. Once the repository is cloned and the project is opened on your Android Studio you can select “Run” on the top bar then select “Run app” a dialog box will then prompt you and ask if you would like to run on a emulator or if your android device is plugged into your computer it will allow you to select the android device. Then hit run. Android Studio will then install the APK on either the emulator or directly onto your phone and run the application once done.
2. Click “Downloads” on the left hand menu. Click and download “RealEyesDemo.apk” this is the apk that can be directly downloaded onto your android device. After downloading it will ask you to install the application (This can only be done if you have allow from unknown sources checked off under your settings).

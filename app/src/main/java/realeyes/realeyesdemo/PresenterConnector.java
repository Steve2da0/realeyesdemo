package realeyes.realeyesdemo;

public interface PresenterConnector {
    // this is the connection to the presenter to pass the video url that was played
    void videoFinished(String urlPlayed);
}

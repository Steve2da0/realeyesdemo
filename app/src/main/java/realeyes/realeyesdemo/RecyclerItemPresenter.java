package realeyes.realeyesdemo;

import java.util.ArrayList;
import java.util.List;

public class RecyclerItemPresenter {

    private final List<String> urls;
    private MainActivity mainActivity;

    public RecyclerItemPresenter(List<String> urls, MainActivity mainActivity) {
        this.urls = urls;
        this.mainActivity = mainActivity;
    }

    public void bindRecyclerItemUpdater(int position, RecyclerItemUpdater updater)
    {
        String currentUrl = urls.get(position);
        updater.setDuration("Video Time Length");
        updater.setTitle("Video Clip: "+(position+1));
        updater.setURL(currentUrl);
    }

    public int getUrlsSize ()
    {
        return urls.size();
    }

    public void videoFinished(String url) {
        int index = urls.indexOf(url);
        if(index == 0) {
            mainActivity.scrollForward();
        } else {
            mainActivity.scrollBack();
        }
    }
}

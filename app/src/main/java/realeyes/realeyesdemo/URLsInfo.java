package realeyes.realeyesdemo;

import java.util.ArrayList;
import java.util.List;

public class URLsInfo {

    private final static String tosTeaser = "https://s3.amazonaws.com/interview-quiz-stuff/tos-trailer/master.m3u8";
    private final static String tosClip = "https://s3.amazonaws.com/interview-quiz-stuff/tos/master.m3u8";

    public static List<String> urlsInfoList ()
    {
        ArrayList<String>urlsInfoList = new ArrayList<>();
        urlsInfoList.add(tosTeaser);
        urlsInfoList.add(tosClip);
        return urlsInfoList;
    }
}

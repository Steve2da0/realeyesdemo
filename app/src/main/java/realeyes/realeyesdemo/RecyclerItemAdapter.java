package realeyes.realeyesdemo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

public class RecyclerItemAdapter extends RecyclerView.Adapter<RecyclerItemViewHolder>{

private final RecyclerItemPresenter presenter;

    public RecyclerItemAdapter(RecyclerItemPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public RecyclerItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mainview, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerItemViewHolder holder, int position) {
        holder.setPresenterConnector(presenter);
        presenter.bindRecyclerItemUpdater(position,holder);
    }

    @Override
    public int getItemCount() {
        return presenter.getUrlsSize();
    }
}

package realeyes.realeyesdemo;

import android.net.sip.SipSession;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerItemPresenter recyclerItemPresenter;
    private RecyclerItemAdapter recyclerItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.recyclerView = findViewById(R.id.mainRecyleView);
        recyclerItemPresenter = new RecyclerItemPresenter(URLsInfo.urlsInfoList(), this);
        recyclerItemAdapter = new RecyclerItemAdapter(recyclerItemPresenter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerItemAdapter);

    }
    // scrolls to the right
    public void scrollForward() {
        recyclerView.smoothScrollToPosition(1);
    }

    // scrolls to the left
    public void scrollBack() {
        recyclerView.smoothScrollToPosition(0);
    }
}

package realeyes.realeyesdemo;

import android.media.MediaPlayer;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

public class RecyclerItemViewHolder extends RecyclerView.ViewHolder implements RecyclerItemUpdater {

    private TextView title;
    private TextView duration;
    private ImageView play;
    private ImageView pause;
    private SeekBar seekBar;
    private VideoView videoView;
    private  String url;
    private TextView currentTime;
    private TextView durationTime;
    private RecyclerItemPresenter presenter;
    private Handler handler = new Handler();
    private int currentPlace = 0;
    private ConstraintLayout videoControlHolder;
    private ConstraintLayout mainview;
    private Runnable seekbarUpdater = new Runnable() {
        @Override
        public void run() {
            String time = getDisplayTime(videoView.getCurrentPosition());
            int update = (videoView.getCurrentPosition()/1000);
            seekBar.setProgress(update);
            currentTime.setText(time);
            handler.postDelayed(this, 1000);
        }
    };

    private String getDisplayTime(int miliDuration) {
        int secondsDuration = (miliDuration/1000)%60;
        int minutesDuration = (miliDuration/1000)/60;
        return minutesDuration+":"+secondsDuration;
    }

    private String getDisplayTimeWithSeconds(int totalSeconds) {
        int seconds = totalSeconds % 60;
        int minutes = totalSeconds/60;
        return minutes + ":" + seconds;
    }

    public void setPresenterConnector(RecyclerItemPresenter presenter) {
        this.presenter = presenter;
    }

    public RecyclerItemViewHolder(View itemView)
    {
        super(itemView);
        this.title = itemView.findViewById(R.id.title);
        this.duration = itemView.findViewById(R.id.duration);
        this.play = itemView.findViewById(R.id.play);
        this.pause = itemView.findViewById(R.id.pause);
        this.seekBar = itemView.findViewById(R.id.seekBar);
        this.videoView = itemView.findViewById(R.id.videoView);
        this.currentTime = itemView.findViewById(R.id.currentTime);
        this.durationTime = itemView.findViewById(R.id.durationTime);
        this.videoControlHolder = itemView.findViewById(R.id.videoControlHolder);
        this.mainview = itemView.findViewById(R.id.mainview);

        this.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                videoView.start();
                int miliDuration = videoView.getDuration();
                String durationDisplayTime = getDisplayTime(miliDuration);
                duration.setText(durationDisplayTime);
                seekBar.setMax(miliDuration/1000);
                durationTime.setText(durationDisplayTime);
                if(currentPlace > 0) {
                    videoView.seekTo(currentPlace);
                    currentPlace = 0;
                }
                handler.post(seekbarUpdater);
            }
        });


        this.videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                presenter.videoFinished(url);
            }
        });

        this.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser) {
                    seekBar.setProgress(progress);
                    currentTime.setText(getDisplayTimeWithSeconds(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                videoView.seekTo(seekBar.getProgress()*1000);
            }
        });

        this.pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.pause();
                pause.setVisibility(View.GONE);
                play.setVisibility(View.VISIBLE);
                handler.removeCallbacks(seekbarUpdater);
                currentPlace = videoView.getCurrentPosition();
            }
        });

        this.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.stopPlayback();
                videoView.setVideoPath(url);
                play.setVisibility(View.GONE);
                pause.setVisibility(View.VISIBLE);
                handler.post(seekbarUpdater);
            }
        });

        this.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(videoControlHolder.getVisibility() == View.INVISIBLE) {
                    videoControlHolder.setVisibility(View.VISIBLE);
                    seekBar.requestFocus();
                } else {
                    videoControlHolder.setVisibility(View.INVISIBLE);
                    seekBar.clearFocus();
                }
            }
        });
    }

    @Override
    public void setURL(String url) {

        this.url = url;
        System.out.println("THE URL IS :"+this.url);
        this.videoView.setVideoPath(this.url);
    }

    @Override
    public void setTitle(String title) {
        this.title.setText(title);
    }

    @Override
    public void setDuration(String duration) {
        this.duration.setText(duration);
    }
}


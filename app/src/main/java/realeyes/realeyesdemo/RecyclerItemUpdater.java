package realeyes.realeyesdemo;

public interface RecyclerItemUpdater  {
    void setURL (String url);
    void setTitle (String title);
    void setDuration (String duration);
}
